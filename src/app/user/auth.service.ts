import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response, Headers, RequestOptions } from '@angular/http';

import { IUser } from './user';
import { MessageService } from '../messages/message.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Injectable()
export class AuthService {
    currentUser: IUser;
    redirectUrl: string;
    
    private baseUrl_filetransfer = 'http://localhost:8080/filetransferservice/api';

    constructor(private http: Http,private messageService: MessageService) { }

    isLoggedIn(): boolean {        
        return !!this.currentUser;
    }

    login(userName: string, password: string): Observable<any> {
        if (!userName || !password) {
            this.messageService.addMessage('Please enter your userName and password');
            return;
        }
        
              
        let headers = new Headers({ 'Authorization': 'Basic '+ btoa(userName+':'+password) });
        let options = new RequestOptions({ headers: headers});
        
         return this.http.get(this.baseUrl_filetransfer + '/login',options)
            .map(this.extractData)
            .do(data => {this.updateCurrentUser(data);})
            .catch(this.handleError);
      //  console.log(userInfo.username);
        // if (userName === 'admin') {
        //     this.currentUser = {
        //         id: 1,
        //         userName: userName,
        //         isAdmin: true
        //     };
        //     this.messageService.addMessage('Admin login');
        //     return;
        // }
        // this.currentUser = {
        //     id: 2,
        //     userName: userName,
        //     isAdmin: false
        // };
        // this.messageService.addMessage(`User: ${this.currentUser.userName} logged in`);
    }
     private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }
    
    private updateCurrentUser(obj:any){
        this.currentUser = {
            "id":1,
            "userName": obj.username,
        "isAdmin": obj.isAdmin};
    }



    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        if(error && error.status == 401){
            return Observable.throw("Authentication failed, Invalid Username or Password.");
        }
        return Observable.throw(error.statusText || 'Server error');
    }

    logout(): void {
        this.currentUser = null;
    }
}
