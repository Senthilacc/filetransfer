import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';

@Component({
    templateUrl: './app/user/login.component.html'
})
export class LoginComponent {
    errorMessage: string;
    pageTitle = 'Log In';
    loading: boolean = false;
    
    constructor(private authService: AuthService,
                private router: Router) {
                    
                    
                 }

    login(loginForm: NgForm) {
        if (loginForm && loginForm.valid) {
             this.loading = true;
            let userName = loginForm.form.value.userName;
            let password = loginForm.form.value.password;
            this.authService.login(userName, password).subscribe(data => {
              if (this.authService.redirectUrl) {
                this.router.navigateByUrl(this.authService.redirectUrl);
            } else {
                this.router.navigate(['/configs']);
            }  
             this.loading = false;
            },
            error => {this.errorMessage = error;  this.loading = false;});

            
        } else {
            this.errorMessage = 'Please enter a user name and password.';
        };
    }
}
