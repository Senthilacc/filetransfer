import { Component, OnInit, Input, Output, ElementRef, EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Observable} from 'rxjs/Rx';

@Component({
    
    selector: 'email',
    
    templateUrl: './app/shared/email.component.html'
    
})
export class EmailComponent implements OnInit{
    @Input() values: string;
    @Input() id: string;
    @Input() name: string;    
    
    @Output() result: EventEmitter<String> = new EventEmitter<String>();
    
    ngOnInit(): void{
        
    }
    
     constructor(private elementRef: ElementRef) {
        const eventStream = Observable.fromEvent(elementRef.nativeElement, 'keyup')
           .map(() => this.values)
            .distinctUntilChanged();

        eventStream.subscribe(input => this.result.emit(input));
    }
}