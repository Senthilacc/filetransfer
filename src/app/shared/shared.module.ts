import { NgModule }  from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { StarComponent } from './star.component';
import {InputDebounceComponent} from './input.debounce.component';
import {EmailComponent} from './email.component';

@NgModule({
  imports: [ CommonModule,FormsModule],
  exports : [
    CommonModule,
    FormsModule,
    StarComponent,
    InputDebounceComponent,
    EmailComponent
  ],
  declarations: [ StarComponent,InputDebounceComponent,EmailComponent ],
})
export class SharedModule { }
