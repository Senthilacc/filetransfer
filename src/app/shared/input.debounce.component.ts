import {Component,OnInit, Input, Output, ElementRef, EventEmitter} from '@angular/core';  
import { CommonModule } from '@angular/common';
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'input-debounce',
    templateUrl: './app/shared/input.debounce.component.html'
})
export class InputDebounceComponent {  
    @Input() placeholder: string;
    @Input() delay: number = 300;
    @Output() value: EventEmitter<String> = new EventEmitter<String>();

    public inputValue: string;

    constructor(private elementRef: ElementRef) {
        const eventStream = Observable.fromEvent(elementRef.nativeElement, 'keyup')
            .map(() => this.inputValue)
            .debounceTime(this.delay)
            .distinctUntilChanged();

        eventStream.subscribe(input => this.value.emit(input));
    }
}