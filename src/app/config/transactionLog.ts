export interface ITransactionLog {
    interfaceId: number,   
    interfaceName: string;
    transferredFileName: string;
    processedStatus: string;
    processedTimestamp: string;    
}
