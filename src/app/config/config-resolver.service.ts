import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import {IConfig} from './config';

import { ConfigService } from './config.service';

@Injectable()
export class ConfigResolver implements Resolve<IConfig> {

    constructor(private configService: ConfigService,
                private router: Router) { }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<IConfig> {
       let id = route.params['id'];
       
       //console.log(JSON.stringify(state));
      
        if (isNaN(+id)) {
            console.log(`Product id was not a number: ${id}`);
            this.router.navigate(['/configs']);
            return Observable.of(null);
        }else{
            console.log(id);
        }
        return this.configService.getConfig(+id)
            .map(test => {
                if (test) {
                    return test[0];
                }
                console.log(`Product was not found: ${id}`);
                this.router.navigate(['/configs']);
                return null;
            })
            .catch(error => {
                console.log(`Retrieval error: ${error}`);
                this.router.navigate(['/configs']);
                return Observable.of(null);
            });
    }
}
