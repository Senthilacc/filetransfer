import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router,NavigationEnd,Event } from '@angular/router';

import { Location } from '@angular/common';

import { MessageService } from '../messages/message.service';

import { IConfig } from './config';

import { ConfigService } from './config.service';

@Component({
    templateUrl: './app/config/config-edit.component.html',
    styleUrls: ['./app/config/config-edit.component.css']
})
export class ConfigEditComponent implements OnInit {
    pageTitle: string = 'File Config Edit';
    errorMessage: string;

    private currentConfig: IConfig;
    private originalConfig: IConfig;
    private dataIsValid: { [key: string]: boolean } = {};

    get isDirty(): boolean {
        return JSON.stringify(this.originalConfig) !== JSON.stringify(this.currentConfig);
    }

    get config(): IConfig {
        return this.currentConfig;
    }
    set config(value: IConfig) {
        this.currentConfig = value;
        // Clone the object to retain a copy
        this.originalConfig = Object.assign({}, value);
    }

    constructor(private _location: Location,
        private route: ActivatedRoute,
        private router: Router,
        private configService: ConfigService,
        private messageService: MessageService) { 
            
     
        }

    ngOnInit(): void {
        // Watch for changes to the resolve data
      //  console.log('Route Data' + JSON.stringify(this.route.data));
        this.route.data.subscribe(data => {
             this.onConfigRetrieved(data['config']);
        });
    }

    onConfigRetrieved(config: IConfig): void {
        this.config = config;

        // Adjust the title
        if (this.config.id === 0) {
            this.pageTitle = 'Add Config';
        } else {
            this.pageTitle = `Edit Config: ${this.config.interfaceName}`;
        }
    }

    deleteProduct(): void {
        if (this.config.id === 0) {
            // Don't delete, it was never saved.
            this.onSaveComplete(`${this.config.interfaceName} was deleted`);
        } else {
            if (confirm(`Really delete the config: ${this.config.interfaceName}?`)) {
                this.configService.deleteProduct(this.config.id)
                    .subscribe(
                        () => this.onSaveComplete(`${this.config.interfaceName} was deleted`),
                        (error: any) => this.errorMessage = <any>error
                    );
            }
        }
    }

    isValid(path: string): boolean {
        this.validate();
        if (path) {
            return this.dataIsValid[path];
        }
        return (this.dataIsValid &&
          Object.keys(this.dataIsValid).every(d => this.dataIsValid[d] === true));
      //      return true;
    }

    saveProduct(): void {        
        if (this.isValid(null)) {
            this.configService.saveProduct(this.config)
                .subscribe(
                    () => this.onSaveComplete(`${this.config.interfaceName} was saved`),
                    (error: any) => this.errorMessage = <any>error
                );
        } else {
            this.errorMessage = 'Please correct the validation errors.';
        }
    }

    onSaveComplete(message?: string): void {
        if (message) {
            this.messageService.addMessage(message);
        }
        this.reset();
        this.router.navigate(['/configs']);     
    }

    back(): void {
        this._location.back();
    }

    // Reset the data
    // Required after a save so the data is no longer seen as dirty.
    reset(): void {
        this.dataIsValid = null;
        this.currentConfig = null;
        this.originalConfig = null;
    }

    validate(): void {
        // Clear the validation object
        this.dataIsValid = {};
        
        if(this.validateInterfaceName() 
        && this.validateSourceAddress() 
        && this.validateDestinationAddress() 
        && this.validateCronExpression()){
            this.dataIsValid['info'] = true;
        }else{
            this.dataIsValid['info'] = false;
        }
        
        if(this.config.emailacknowledgementRequired && 
        this.config.emailacknowledgementRequired == true &&
        this.validateFromAddress() 
        && this.validateToAddress()){
            this.dataIsValid['tags'] = true;
        }else if(this.config.emailacknowledgementRequired && 
        !this.config.emailacknowledgementRequired && 
        (this.isNotEmpty(this.config.emailInfo.fromAddr) || this.isNotEmpty(this.config.emailInfo.toAddr))
        ){
            this.dataIsValid['tags'] = false;
        }else if(this.config.emailacknowledgementRequired && 
        this.config.emailacknowledgementRequired == true &&
       (!this.isNotEmpty(this.config.emailInfo.fromAddr) || !this.isNotEmpty(this.config.emailInfo.toAddr))){
            this.dataIsValid['tags'] = false;
        }else{
            this.dataIsValid['tags'] = true;
        }

          }
    
    validateInterfaceName(): boolean {
        return this.isNotEmpty(this.config.interfaceName) && this.config.interfaceName.length >= 5;
    }
    
    validateSourceAddress(): boolean {
       return this.isNotEmpty(this.config.sourceAddress);
    }
    
    validateDestinationAddress(): boolean {
 
        return this.isNotEmpty(this.config.destinationAddress);
    }
    
    validateCronExpression(): boolean {      
        return this.isNotEmpty(this.config.cronExpression);
    }
    
    validateFromAddress(): boolean {
        return this.isNotEmpty(this.config.emailInfo.fromAddr);
    }
    
    validateToAddress(): boolean {
        return this.isNotEmpty(this.config.emailInfo.toAddr);
    }
    
     
    isNotEmpty(obj: any): boolean {
         if(obj && String(obj).trim.length >= 0){
            return true;
        }
        return false;
    }
}
