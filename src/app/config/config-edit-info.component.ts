import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import {IConfig} from './config';


@Component({
    templateUrl: './app/config/config-edit-info.component.html',
    styleUrls: ['./app/config/config-edit.component.css']
})
export class ConfigEditInfoComponent implements OnInit {
    @ViewChild(NgForm) productForm: NgForm;

    errorMessage: string;
    config: IConfig;

    constructor(private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.route.parent.data.subscribe(data => {
            this.config = data['config'];

            if (this.productForm) {
                this.productForm.reset();
            }
        });
    }
}
