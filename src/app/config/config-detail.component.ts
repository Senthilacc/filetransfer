import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IConfig } from './config';
import { IEmailConfig } from './emailConfig';


@Component({
    templateUrl: './app/config/config-detail.component.html',
    styleUrls: ['./app/config/config-detail.component.css']
})
export class ConfigDetailComponent implements OnInit {
    pageTitle: string = 'File Config Details';
    config: IConfig;
    emailInfo: IEmailConfig;
    errorMessage: string;
    containsEmailConfig: boolean = false;
    showFileConfigurationTab: boolean = true;

    constructor(private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.config = this.route.snapshot.data['config'];
        if(this.config.emailInfo && this.config.emailInfo.toAddr != null){
            this.containsEmailConfig = true;
            this.emailInfo = this.config.emailInfo;
        }
    }
    toggleTab(currentTab: string): void {
       if(this.containsEmailConfig)
           this.showFileConfigurationTab=!this.showFileConfigurationTab;
    
    }
}
