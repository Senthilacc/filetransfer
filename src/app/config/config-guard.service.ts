import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';

import { ConfigEditComponent } from './config-edit.component';

@Injectable()
export class ConfigEditGuard implements CanDeactivate<ConfigEditComponent> {

    canDeactivate(component: ConfigEditComponent): boolean {
        if (component.isDirty) {
            let interfaceName = component.config.interfaceName || 'New Config';
            return confirm(`Navigate away and lose all changes to ${interfaceName}?`);
        }
        return true;
    }
}
