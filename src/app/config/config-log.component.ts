import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ITransactionLog } from './transactionLog';
import { ConfigService } from './config.service';

import { IConfig } from './config';


@Component({
    templateUrl: './app/config/config-log.component.html'
})
export class ConfigLogComponent implements OnInit {
    pageTitle: string = 'Transaction Log Details';  
    config: IConfig;
    
    logs: ITransactionLog[] = new Array<ITransactionLog>();

    constructor(private route: ActivatedRoute,
    private configService: ConfigService) {
        this.config = this.route.snapshot.data['config'];
       
        this.configService.searchLogsByInterfaceId(this.route.params['_value'].id).subscribe( data =>
            {
                console.log("Logs:"+JSON.stringify(data));
                this.logs = data;
            }
        );
     }

    ngOnInit(): void {
       
        
    }
    
}
