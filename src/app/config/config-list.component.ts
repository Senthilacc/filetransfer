import { Component, OnInit }  from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IConfig } from './config';
import { ConfigService } from './config.service';

import {NgxPaginationModule} from 'ngx-pagination';

@Component({
    templateUrl: './app/config/config-list.component.html',
    styleUrls: ['./app/config/config-list.component.css']
})
export class ConfigListComponent implements OnInit {
    pageTitle: string = 'Search Config';
    imageWidth: number = 50;
    imageMargin: number = 2;
    showImage: boolean = false;
    listFilter: string;
    errorMessage: string;
    filteredValue: string;
    loading: boolean;
    products: IConfig[];

    constructor(private productService: ConfigService,
                private route: ActivatedRoute) {
                    this.loading = true;
                      this.productService.getConfigs()
                .subscribe(products => {this.loading = false; this.products = products},
                           error => {this.loading = false; this.errorMessage = <any>error});
                 }

    toggleImage(): void {
        this.showImage = !this.showImage;
    }

    ngOnInit(): void { 
        this.listFilter = this.route.snapshot.queryParams['filterBy'] || '';
        this.showImage = (this.route.snapshot.queryParams['showImage'] === 'true');
        // console.log(this.route.snapshot.queryParamMap.get('filterBy'));            

      
    }
    
    searchConfig(value: string): void{
        this.filteredValue = value;
        this.loading = true;
          this.productService.getConfigByName(value)
                .subscribe(products => { this.products = products; this.loading=false;},
                           error => {this.errorMessage = <any>error;
                               this.loading = false;});
    }
    
    reconfigure(id: number): void {
        
        this.productService.reconfigure(id).subscribe(
            (data: any) => console.log(JSON.stringify(data)));
    }
   
}
