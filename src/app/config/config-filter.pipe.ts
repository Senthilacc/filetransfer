import {  PipeTransform, Pipe } from '@angular/core';

import { IConfig } from './config';

@Pipe({
    name: 'configFilter'
})
export class ConfigFilterPipe implements PipeTransform {

    transform(value: IConfig[], filterBy: string): IConfig[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        return filterBy ? value.filter((config: IConfig) =>
            config.interfaceName.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }
}
