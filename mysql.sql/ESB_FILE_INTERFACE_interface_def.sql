-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: ESB_FILE_INTERFACE
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `interface_def`
--

DROP TABLE IF EXISTS `interface_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interface_def` (
  `interface_id` int(11) NOT NULL AUTO_INCREMENT,
  `interface_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `archiveAddress` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `active_ind` char(1) DEFAULT NULL,
  `source` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `destination` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `localArchiveAddress` varchar(500) CHARACTER SET utf8 NOT NULL,
  `cron_expression` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `processMultipleFiles` char(1) NOT NULL,
  `lastsuccessfultimestamp` datetime(3) DEFAULT NULL,
  `regexFileNameFilterExpression` varchar(70) CHARACTER SET utf8 DEFAULT NULL,
  `compressFileBeforeTransfer` char(1) DEFAULT NULL,
  `renameFileSuffixPattern` char(50) CHARACTER SET utf8 DEFAULT NULL,
  `emailacknowledgementRequired` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`interface_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interface_def`
--

LOCK TABLES `interface_def` WRITE;
/*!40000 ALTER TABLE `interface_def` DISABLE KEYS */;
INSERT INTO `interface_def` VALUES (1,'GILMORE-INCOMING','file:////sap-qaerp1/interfaces/Gilmore/Out ','Y','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','0 0/1 15 1/1 * ? *','N',NULL,NULL,NULL,NULL,1),(2,'GILMORE-INCOMING-1','file:////sap-qaerp1/interfaces/Gilmore/Out ','Y','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','0 0/1 15 1/1 * ? *','N',NULL,NULL,NULL,NULL,1),(3,'GILMORE-INCOMING-2','file:////sap-qaerp1/interfaces/Gilmore/Out ','Y','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','0 0/1 15 1/1 * ? *','N',NULL,NULL,NULL,NULL,1),(4,'GILMORE-INCOMING-3','file:////sap-qaerp1/interfaces/Gilmore/Out ','Y','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','0 0/1 15 1/1 * ? *','N',NULL,NULL,NULL,NULL,1),(5,'-INCOMING-3','file:////sap-qaerp1/interfaces/Gilmore/Out ','Y','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','0 0/1 15 1/1 * ? *','N',NULL,NULL,NULL,NULL,1),(6,'-INCOMING-1','file:////sap-qaerp1/interfaces/Gilmore/Out ','Y','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','sftp://caascosap:!jUfT24K@upload.gilmore.ca/TEST_IN ','0 0/1 15 1/1 * ? *','N',NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `interface_def` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-24  0:27:46
